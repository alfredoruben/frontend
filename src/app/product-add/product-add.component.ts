import { Component, OnInit, Input, DebugElement } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

  @Input() productData = { nombreUsuario:'', password: ''};

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  addProduct() {
    this.rest.addProduct(this.productData).subscribe((result) => {
      debugger;

      if (typeof(Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        // Store value
        localStorage.setItem("token", result.token);
       // Retrieve value from local storage and assign to variable
       var myId = localStorage.getItem("token");
      } else {
          // Sorry! No Web Storage support..
      }

      this.router.navigate(['/products/']);
    }, (err) => {
      console.log(err);
    });
  }

}
